// FindWpdDevices.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#define INITGUID
#include <initguid.h>
#include <SetupAPI.h>
#include <Devpropdef.h>
#include <Devpkey.h>
#include <Cfg.h>
#include <Usbiodef.h>
#include <Cfgmgr32.h>
#include <atlstr.h>
#include <PortableDevice.h>

#pragma comment(lib, "setupapi.lib")
#pragma comment(lib, "Cfgmgr32.lib")

int nLabel = 0;

VOID
Oops(
__in PCTSTR File,
ULONG Line,
DWORD dwError)
{
	CString str;

	str.Format(_T("File: %s, Line %d, Error %d\n"), File, Line, dwError);
	CString s;
	s.Format(_T("[%04d] %s\n"), nLabel, str);
	OutputDebugString(s);
}


void logIt(TCHAR* fmt, ...)
{
	va_list args;
	va_start(args, fmt);	
	CString str;
	str.Format(fmt, args);
	va_end(args);

	CString s;
	s.Format(_T("[%04d] %s\n"), nLabel, str);
	OutputDebugString(s);
}

void logHex(BYTE* buffer, size_t buf_sz)
{
	TCHAR line_buf[75];
	size_t pos = 0;
	int line_count = 0;
	while (pos<buf_sz)
	{
		int line_pos_1 = 0;
		int line_pos_2 = 0;
		ZeroMemory(line_buf, sizeof(line_buf));
		_stprintf_s(line_buf, _T("%04d:"), line_count);
		line_buf[5] = ' ';
		line_pos_1 = 6;
		line_pos_2 = 54;
		for (size_t i = 0; i < 16 && pos<buf_sz; i++, pos++)
		{
			_stprintf_s(&line_buf[line_pos_1], 3, _T("%02x"), buffer[pos]);
			line_pos_1 += 2;
			line_buf[line_pos_1] = ' ';
			line_pos_1++;
			if (isprint(buffer[pos]))
				line_buf[line_pos_2] = (char)buffer[pos];
			else
				line_buf[line_pos_2] = '.';
			line_pos_2++;
		}
		for (; line_pos_1 < 54; line_pos_1++)
		{
			line_buf[line_pos_1] = ' ';
		}
		line_buf[line_pos_2] = 0;
		logIt(_T("%s\n"), line_buf);
		line_count++;
	}
}

#define OOPS()		Oops(_T(__FILE__), __LINE__, GetLastError())
#define OOPSERR(d)	Oops(_T(__FILE__), __LINE__, d)

int GetDeviceStatus(TCHAR *sName, int nPort, STRING &sLocation)
{
	int ret = ERROR_UNKNOWN_FEATURE;
	if (sName == NULL || _tcslen(sName) == 0 || nPort == 0)
		return ERROR_INVALID_PARAMETER;

	TCHAR symblName[1024] = { 0 };

	if ((_tcsncicmp(sName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(sName, _T("\\\\.\\"), 4) == 0))
	{
		_stprintf_s(symblName, _T("%s"), sName);
	}
	else
	{
		_stprintf_s(symblName, _T("%s%s"), _T("\\\\?\\"), sName);
	}

	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		DWORD sz = 0;
		SP_DEVICE_INTERFACE_DATA devIntData;
		devIntData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		if (SetupDiOpenDeviceInterface(hDevInfo, symblName, 0, &devIntData))
		{
			DEVPROPTYPE type;
			BYTE b[2048];
			ZeroMemory(b, sizeof(b));
			sz = 0;

			SP_DEVINFO_DATA devInfoData = { 0 };
			devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
			SetupDiGetDeviceInterfaceDetail(hDevInfo, &devIntData, NULL, 0, &sz, &devInfoData);
			if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
			{
				if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_LocationPaths, &type, b, 2048, &sz, 0))
				{
					//logIt(_T("Property: #%d\n"), i + 1);
					//dump(p, type, b, sz);
					if (type == DEVPROP_TYPE_STRING_LIST)
					{
						TCHAR *instanceid = (TCHAR *)b;
						DWORD dwoffset = 0;
						BOOL bFind = FALSE;
						if (instanceid != NULL && _tcslen(instanceid)>0)
						{
							logIt(instanceid);

							sLocation = instanceid;
							ret = ERROR_SUCCESS;
						}

					}
				}
				else
				{
					ret = GetLastError();
					OOPSERR(ret);
				}
			}
			else
			{
				ret = GetLastError();
				OOPSERR(ret);
			}
		}
		else
		{
			ret = GetLastError();
			OOPSERR(ret);
		}

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
	{
		ret = GetLastError();
		OOPSERR(ret);
	}

	return ret;
}

//GUID_DEVINTERFACE_WPD
//GUID_DEVINTERFACE_WPD_SERVICE
int EnumerateWPDDeviceInterfaces(std::map<STRING, STRING> &locpthSynbl)
{
	int ret = ERROR_SUCCESS;
	SP_DEVICE_INTERFACE_DATA deviceInterfaceData;
	DWORD required = 0;
	deviceInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

	int nBufferSize = 0;

	SP_DEVINFO_DATA devInfoData = { 0 };
	devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

	DWORD MemberIndex = -1;
	BOOL  Result;


	HDEVINFO hDevHandle = SetupDiGetClassDevs(&GUID_DEVINTERFACE_WPD, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
	if (hDevHandle != INVALID_HANDLE_VALUE)
	{
		TCHAR *buffer = NULL;
		PSP_DEVICE_INTERFACE_DETAIL_DATA devicedetailData;
		do
		{
			MemberIndex++;
			Result = SetupDiEnumDeviceInterfaces(hDevHandle, 0, &GUID_DEVINTERFACE_WPD, MemberIndex, &deviceInterfaceData);
			if (Result)
			{
				SetupDiGetDeviceInterfaceDetail(hDevHandle, &deviceInterfaceData, NULL, 0, &required, NULL);
				if (GetLastError() == ERROR_INSUFFICIENT_BUFFER && required > 0)
				{
					buffer = new TCHAR[required];
					if (buffer != NULL)
					{
						devicedetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)buffer;
						devicedetailData->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);
						STRING symbl;
						if (SetupDiGetDeviceInterfaceDetail(hDevHandle, &deviceInterfaceData, devicedetailData, required, &required, &devInfoData))
						{
							symbl = devicedetailData->DevicePath;
							DWORD sz = 0;
							DEVPROPTYPE type;
							BYTE b[2048];
							ZeroMemory(b, sizeof(b));

							sz = 0;
							STRING skey;

							if (SetupDiGetDeviceProperty(hDevHandle, &devInfoData, &DEVPKEY_Device_LocationPaths, &type, b, 2048, &sz, 0))
							{
								if (type == DEVPROP_TYPE_STRING_LIST)
								{
									TCHAR *instanceid = (TCHAR *)b;
									//DWORD dwoffset = 0;
									if (instanceid != NULL && _tcslen(instanceid)>0)
									{
										logIt(instanceid);
										skey = instanceid;
										//break;
										//instanceid += _tcslen(instanceid) + 1;
										locpthSynbl.insert(std::make_pair(skey, symbl));
									}
								}
							}
							else
							{
								logIt(_T("Can not SetupDiGetDeviceProperty %d"), GetLastError());
								continue;
							}

							ret = ERROR_SUCCESS;
						}
						else
						{
							ret = GetLastError();
							OOPSERR(ret);
						}
						delete[]buffer;
						buffer = NULL;
					}
					else
					{
						logIt(_T("Alloc memery failed.\n"));
						ret = ERROR_NOT_ENOUGH_MEMORY;
						OOPSERR(ret);
					}
				}
				else
				{
					ret = GetLastError();
					OOPSERR(ret);
				}
			}
			else
			{
				ret = GetLastError();
				OOPSERR(ret);
			}

		} while (ret != ERROR_NO_MORE_ITEMS);
	}
	else
		ret = GetLastError();

	return ret;
}

LONG WINAPI my_UnhandledExceptionFilter(struct _EXCEPTION_POINTERS* ExceptionInfo)
{
	LONG ret = EXCEPTION_EXECUTE_HANDLER;
	logIt(_T("FindWpdDevices unhandled exception: called. exceptionCode: %x, exceptionAddress: %x \n"), ExceptionInfo->ExceptionRecord->ExceptionCode, ExceptionInfo->ExceptionRecord->ExceptionAddress);
	
	return ret;
}


int _tmain(int argc, _TCHAR* argv[])
{
	SetUnhandledExceptionFilter(my_UnhandledExceptionFilter);
	//_CrtSetReportMode(_CRT_ERROR | _CRT_ASSERT | _CRT_WARN, 0);

	int hubport = 0;
	STRING hubname;

	for (int i = 0; i < argc; i++)
	{
		if (argv[i] != NULL && argv[i][0] == L'-' || argv[i][0] == L'/')
		{
			TCHAR* v = _tcschr(argv[i], L'=');
			if (v != NULL) v++;
			if (_tcsncicmp(&argv[i][1], _T("label"), 5) == 0)
			{
				nLabel = _ttoi(v);
			}
			else if (_tcsncicmp(&argv[i][1], _T("hubport"), 7) == 0)
			{
				hubport = _ttoi(v);
			}
			if (_tcsncicmp(&argv[i][1], _T("hubname"), 7) == 0)
			{
				hubname = v;
			}
		}
	}

	if (hubport == 0 || hubname.empty())
	{
		_tprintf(_T("command line error.\n"));
		_tprintf(_T("-label=\\d+ -hubport=\\d+ -hubname=\\s+\n"));
		return -1;
	}

	std::map<STRING, STRING> locpthSynbl;
	STRING sLocation;
	if (GetDeviceStatus((TCHAR *)hubname.c_str(), hubport, sLocation)==ERROR_SUCCESS)
	{
		TCHAR sLabelData[256] = {0};
		_stprintf_s(sLabelData, _T("#USB(%d)"), hubport);

		sLocation.append(sLabelData);
		EnumerateWPDDeviceInterfaces(locpthSynbl);
		std::map<STRING, STRING>::iterator it = locpthSynbl.begin();
		for (; it != locpthSynbl.end(); ++it)
		{
			STRING sLoc = it->first;
			if (sLoc.find(sLocation) == 0)
			{
				_tprintf(it->second.c_str()); _tprintf(_T("\n"));
			}
		}
	}
	return 0;
}

